# Description
This repository contains pipeline files to track a lore-email list at
lore.kernel.org.  Every time there is a stable-rc review email that will
trigger the [trigger a linux-stable-rc
pipeline](https://gitlab.com/Linaro/lkft/triggers/linux-stable-rc-pipeline) and
start LKFT's release run for that kernel.

## Variables
Variables which require to be setup in CI/CD -> variables are 'GITLAB_PIPELINE_TRIGGER_TOKEN' and 'TRIGGER_PROJECT_ID'.
The 'GITLAB_PIPELINE_TRIGGER_TOKEN' should be setup from the remote trigger project under 'Settings->CI/CD->"Pipeline trigger tokens"'.
