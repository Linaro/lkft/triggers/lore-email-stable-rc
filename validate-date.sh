#!/bin/bash

usage() {
    echo "Usage: $0 '<date>'"
    echo "  <date>: The date to check in the format 'Fri Jan 24 09:28:47 2025 +0000'"
    exit 1
}

if [ "$#" -lt 1 ]; then
    usage
fi

input_date="$1"

input_date=$(echo "${input_date}" | awk '{$NF=""; print $0}')
# Extract the date part (ignoring time and timezone) and format it to YYYY-MM-DD
formatted_input_date=$(date -d "$input_date" +"%Y-%m-%d" 2>/dev/null)

if [ -z "$formatted_input_date" ]; then
    echo "Invalid date format. Please provide a valid date in the format 'Fri Jan 24 09:28:47 2025 +0000'."
    exit 1
fi

today=$(date +"%Y-%m-%d")
yesterday=$(date -d "yesterday" +"%Y-%m-%d")

if [ "$formatted_input_date" == "$today" ]; then
    day="today"
elif [ "$formatted_input_date" == "$yesterday" ]; then
    day="yesterday"
else
    echo "The date does not match today or yesterday."
    exit 1
fi

echo "The date matches ${day}'s date."
exit 0
